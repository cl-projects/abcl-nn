;;;; abcl-nn.lisp

(in-package #:abcl-nn)


(defun create-neural-network (layers)
  (java:jnew "jsat.classifiers.neuralnetwork.DReDNetSimple"
             (java:jnew-array-from-list "int" layers)))


;; (create-neural-network '(1000 333 777)) => neural-network


(defun load-data-set (arrf-filename)
  (java:jstatic (java:jmethod "jsat.ARFFLoader" "loadArffFile" "java.io.File")
                "jsat.ARFFLoader"
                (java:jnew "java.io.File" arrf-filename)))


;; (load-data-set "/home/alejandrozf/projects/abcl-nn/iris.arff") => dataset


(defun train (neural-network dataset)
  (let ((classification-dataset (java:jnew "jsat.classifiers.ClassificationDataSet" dataset 0)))
    (java:jcall (java:jmethod "jsat.classifiers.neuralnetwork.DReDNetSimple"
                              "trainC" "jsat.classifiers.ClassificationDataSet")
                neural-network classification-dataset)))


(defun classify (trained-neural-network data-point)
  (java:jcall (java:jmethod "jsat.classifiers.neuralnetwork.DReDNetSimple"
                            "classify" "jsat.classifiers.DataPoint")
              trained-neural-network
              data-point))
