# abcl-nn
### _alejandrozf_

ABCL neural network library (using JSAT)

Example of usage:

```
CL-USER> (ql:quickload :abcl-nn)
To load "abcl-nn":
  Load 1 ASDF system:
    abcl-nn
; Loading "abcl-nn"

(:ABCL-NN)
CL-USER> (let* ((dataset-filename (namestring (asdf/system:system-relative-pathname
                                              :abcl-nn "example-data/iris.arff")))
                (nn (abcl-nn:create-neural-network '(5 7 3)))
                (dataset (abcl-nn:load-data-set dataset-filename)))
           ;; train
           (abcl-nn:train nn dataset)

           ;; classify
           (let* ((c-dataset (java:jnew "jsat.classifiers.ClassificationDataSet" dataset 0))
                  (data-point (java:jcall "getDataPoint" c-dataset 1)))
             (abcl-nn:classify nn data-point)))

#<jsat.classifiers.CategoricalResults [0.33329919806129304, 0.33336697.... {77E2E19F}>
CL-USER>
```

## License

MIT
