;;;; abcl-nn.asd

(asdf:defsystem #:abcl-nn
  :description "ABCL neural network library (using JSAT)"
  :author "alejandrozf"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:mvn "com.edwardraff/JSAT/0.0.9")
               (:file "package")
               (:file "abcl-nn")))
