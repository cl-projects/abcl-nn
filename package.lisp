;;;; package.lisp

(defpackage #:abcl-nn
  (:use #:cl)
  (:export #:create-neural-network
           #:load-data-set
           #:train
           #:classify))
